<?php
/**
 *  This is a database entry point.
 *
 * This class provides persistent manager for rest of the application.
 * Database login is stored in this class
 *
 * @package      server
 * @subpackage   db
 * @category     PersistenceProvider
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 */

$loginDsn = "mysql:dbname=hamsatom";
$loginUsername = "hamsatom";
$loginPassword = "webove aplikace";

// PDO $db Persistence unit that provides connection to database
/**
 * Persistence unit that provides connection to database
 *
 * @global PDO $db persistence unit for communication with database
 * @var PDO $db persistence unit for communication with database
 */
$db = new PDO($loginDsn, $loginUsername, $loginPassword, array(
    PDO::ATTR_PERSISTENT => true, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
));

