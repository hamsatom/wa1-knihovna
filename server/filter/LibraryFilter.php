<?php
/**
 * This is a filter for Library entity
 *
 * This class is used as a entry point for ajax request.
 * This class calls generic filter and echoes result.
 *
 * @package      server
 * @subpackage   filter
 * @category     Filter
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses getLimit(), doLibraryCount(), getPageNumber()
 * @api
 */

require('./Filter.php');
require('./../Utils.php');

$filterLimit = getLimit("libraryLimit");
$filterMaxPage = doLibraryCount($filterLimit);
$filterPage = getPageNumber("libraryPage", $filterMaxPage);

echo doLibraryFilter($filterPage - 1, $filterLimit);