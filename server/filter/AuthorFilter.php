<?php
/**
 * This is a filter for Author entity
 *
 * This class is used as a entry point for ajax request.
 * This class calls generic filter and echoes result.
 *
 * @package      server
 * @subpackage   filter
 * @category     Filter
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses getLimit(), doAuthorCount(), getPageNumber()
 * @api
 */

require('./Filter.php');
require('./../Utils.php');

$filterLimit = getLimit("authorLimit");
$filterMaxPage = doAuthorCount($filterLimit);
$filterPage = getPageNumber("authorPage", $filterMaxPage);

echo doAuthorFilter($filterPage - 1, $filterLimit);