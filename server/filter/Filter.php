<?php
/**
 * This is a generic filter for all entities
 *
 * This class is used as a service for filter and count database operation
 *
 * @package      server
 * @subpackage   filter
 * @category     Filter
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses DB.php, db
 */

/**
 * Applies filter on author records and returns found records as tbody hrml
 *
 * @param int $pageStart index of first record. This value will be used in LIMIT query
 * @param $limit number of results. This value will be used in LIMIT query
 * @return string tbody html if select succeeded, error message otherwise
 * @uses _createQuery(), _doFilter()
 */
function doAuthorFilter($pageStart, $limit)
{
    $fields = array("first_name", "surname", "email");
    $query = _createQuery($fields);
    return _doFilter("AUTHORS", $query, $fields, $pageStart, $limit);
}

/**
 * Applies filter on book records and returns found records as tbody hrml
 *
 * @param int $pageStart index of first record. This value will be used in LIMIT query
 * @param int $limit number of results. This value will be used in LIMIT query
 * @return string tbody html if select succeeded, error message otherwise
 * @uses _createQuery(), _doFilter()
 */
function doBookFilter($pageStart, $limit)
{
    $fields = array("name", "isbn", "date", "genre");
    $query = _createQuery($fields);
    return _doFilter("BOOKS", $query, $fields, $pageStart, $limit);
}

/**
 * Applies filter on library records and returns found records as tbody hrml
 *
 * @param int $pageStart index of first record. This value will be used in LIMIT query
 * @param int $limit number of results. This value will be used in LIMIT query
 * @return string tbody html if select succeeded, error message otherwise
 * @uses _createQuery(), _doFilter()
 */
function doLibraryFilter($pageStart, $limit)
{
    $fields = array("name", "address");
    $query = _createQuery($fields);
    return _doFilter("LIBRARIES", $query, $fields, $pageStart, $limit);
}

/**
 * Applies filter on publisher records and returns found records as tbody hrml
 *
 * @param int $pageStart index of first record. This value will be used in LIMIT query
 * @param $limit number of results. This value will be used in LIMIT query
 * @return string tbody html if select succeeded, error message otherwise
 * @uses _createQuery(), _doFilter()
 */
function doPublisherFilter($pageStart, $limit)
{
    $fields = array("name", "address");
    $query = _createQuery($fields);
    return _doFilter("PUBLISHERS", $query, $fields, $pageStart, $limit);
}

/**
 * Count how many pages there can be. The value is taken from result rows after applying author filter
 *
 * @param int $limit Number of records that can be displayed on one page
 * @return float|string The highest possible page number if count succeeded, error message otherwise
 * @uses _createQuery(), _doCount()
 */
function doAuthorCount($limit)
{
    $query = _createQuery(array("first_name", "surname", "email"));
    return _doCount("AUTHORS", $query, $limit);
}

/**
 * Count how many pages there can be. The value is taken from result rows after applying book filter
 *
 * @param $limit int Number of records that can be displayed on one page
 * @return float|string The highest possible page number if count succeeded, error message otherwise
 * @uses _createQuery(), _doCount()
 */
function doBookCount($limit)
{
    $query = _createQuery(array("name", "isbn", "date", "genre"));
    return _doCount("BOOKS", $query, $limit);
}

/**
 * Count how many pages there can be. The value is taken from result rows after applying library filter
 *
 * @param $limit int Number of records that can be displayed on one page
 * @return float|string The highest possible page number if count succeeded, error message otherwise
 * @uses _createQuery(), _doCount()
 */
function doLibraryCount($limit)
{
    $query = _createQuery(array("name", "address"));
    return _doCount("LIBRARIES", $query, $limit);
}

/**
 * Count how many pages there can be. The value is taken from result rows after applying publisher filter
 *
 * @param $limit int Number of records that can be displayed on one page
 * @return float|string The highest possible page number if count succeeded, error message otherwise
 * @uses _createQuery(), _doCount()
 */
function doPublisherCount($limit)
{
    $query = _createQuery(array("name", "address"));
    return _doCount("PUBLISHERS", $query, $limit);
}

/**
 * Dynamically creates SQL query that uses values from GET[field] if the value is present & not empty
 *
 * @param array $fields Name of parameters in GET that should be included in query
 * @return string SQL query that will do desired filter. Empty string if no filter fields found in GET.
 * @access private
 * @uses DB.php, db
 */
function _createQuery(Array $fields)
{
    try {
        require('./../../server/db/DB.php');

        $query = "";
        for ($i = 0; $i < count($fields); $i++) {
            $field = $fields[$i];
            if (isset($_GET[$field]) && !empty($_GET[$field])) {
                if (!empty($query)) {
                    $query = $query . " AND ";
                }
                $query = $query . $field . " LIKE " . $db->quote('%' . $_GET[$field] . '%');
            }
        }

        if (!empty($query)) {
            $query = " WHERE " . $query;
        }

        return $query;

    } catch (PDOException $e) {
        return "Tvorba query selhala";
    }
}

/**
 * Counts number of result rows in database after applying filter. Than counts number of possible pages like ceil(rows/limit)
 *
 * @param $tableName string Name of table where we will do count
 * @param $query string Filter SQL query to include in statement
 * @param $limit int Number of records that can be displayed on page
 * @return float|string The highest possible page number for desired table if succeeded, error message otherwise
 * @access private
 * @uses DB.php, db
 */
function _doCount($tableName, $query, $limit)
{
    try {
        require('./../../server/db/DB.php');

        $result = $db->query("SELECT COUNT(*) FROM $tableName$query");
        $rows = $result->fetchAll();
        return ceil($rows[0]["COUNT(*)"] / $limit);

    } catch (PDOException $e) {
        return "Počítání stránek " . $tableName . " selhalo";
    }
}

/**
 * Selects are records from provided table that match provided SQL query filter and that are on index pageStart and further. Amount of records is at max same as limit.
 *
 * @param $tableName string Name of table from which we are selecting
 * @param $query string Filtering SQL statement
 * @param array $fields Fields that should be present in result and are present as columns in result table
 * @param $pageStart int Index of the first record we should find. This values is used in LIMIT
 * @param $limit int Maximal number of selected rows. This value is used in LIMIT
 * @return string html tbody with results if succeeded, error message otherwise
 * @access private
 * @uses DB.php, db
 */
function _doFilter($tableName, $query, Array $fields, $pageStart, $limit)
{
    try {
        require('./../../server/db/DB.php');

        $result = $db->query("SELECT * FROM $tableName$query LIMIT $pageStart, $limit");

        $table = "";
        foreach ($result as $row) {
            $table = $table . "<tr>";
            for ($i = 0; $i < count($fields); $i++) {
                $field = $fields[$i];
                $table = $table . "<td>" . htmlspecialchars($row[$field], ENT_QUOTES) . "</td>";
            }
            $table = $table . "</tr>";
        }

        return $table;

    } catch (PDOException $e) {
        return "Filtrování " . $tableName . " selhalo";
    }
}