<?php
/**
 * This is a filter for Book entity
 *
 * This class is used as a entry point for ajax request.
 * This class calls generic filter and echoes result.
 *
 * @package      server
 * @subpackage   filter
 * @category     Filter
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses getLimit(), doBookCount(), getPageNumber()
 * @api
 */

require('./Filter.php');
require('./../Utils.php');

$filterLimit = getLimit("bookLimit");
$filterMaxPage = doBookCount($filterLimit);
$filterPage = getPageNumber("bookPage", $filterMaxPage);

echo doBookFilter($filterPage - 1, $filterLimit);