<?php
/**
 * This is a filter for Publisher entity
 *
 * This class is used as a entry point for ajax request.
 * This class calls generic filter and echoes result.
 *
 * @package      server
 * @subpackage   filter
 * @category     Filter
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses getLimit(), doPublisherCount(), getPageNumber()
 * @api
 */

require('./Filter.php');
require('./../Utils.php');

$filterLimit = getLimit("publisherLimit");
$filterMaxPage = doPublisherCount($filterLimit);
$filterPage = getPageNumber("publisherPage", $filterMaxPage);

echo doPublisherFilter($filterPage - 1, $filterLimit);