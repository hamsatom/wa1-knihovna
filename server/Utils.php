<?php
/**
 * This is a utility class for common operations in views
 *
 * @package      server
 * @category     Utility
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses Service.php
 */

/**
 * Return number of records that can be displayed on page. This value is set to cookie and is taken either from GET or cookie.
 *
 * @param $cookieName string Name of cookie which specifies limit of desired page
 * @return int number of records that can be displayed on page, default values is 10
 */
function getLimit($cookieName)
{
    $limit = 10;
    if (isset($_GET["limit"]) && is_numeric($_GET["limit"]) && $_GET["limit"] > 0) {
        $limit = $_GET["limit"];
        setcookie($cookieName, $limit, 0);
    } else if (isset($_COOKIE[$cookieName]) && is_numeric($_COOKIE[$cookieName]) && $_COOKIE[$cookieName] > 0) {
        $limit = $_COOKIE[$cookieName];
    }

    return $limit;
}

/**
 * Verifies that user is logged in and present in user database. If the user is not logged than this method redirects to index.php
 *
 * @uses userExists()
 */
function checkLogin()
{
    session_start();
    require_once(__DIR__ . '/service/Service.php');
    if (!isset($_COOKIE["sessionId"]) || !isset($_SESSION[$_COOKIE["sessionId"]]) || !userExists($_SESSION[$_COOKIE["sessionId"]])) {
        if (isset($_COOKIE["sessionId"])) {
            unset($_COOKIE["sessionId"]);
        }
        header('Location: ./../../index.php', true, 303);
        die();
    }
}

/**
 * Returns number of page where user wants to advance or 1 if no valid page number is set. This value is taken from GET and set to a cookie or taken from cookie if not present in GET.
 *
 * @param $pageName string Name of cookie field that identifies the value. If new value is found than the new value is written to a cookie under this name.
 * @param $maxPage int The biggest valid value of page
 * @return int number of page where user wants to advance or 1 if no valid page number is set
 */
function getPageNumber($pageName, $maxPage)
{
    $page = 1;
    if (isset($_GET["page"]) && is_numeric($_GET["page"]) && $_GET["page"] > 0 && $_GET["page"] <= $maxPage) {
        $page = $_GET["page"];
        setcookie($pageName, $page, 0);
    } else if (isset($_COOKIE[$pageName]) && is_numeric($_COOKIE[$pageName]) && $_COOKIE[$pageName] > 0 && $_COOKIE[$pageName] <= $maxPage) {
        $page = $_COOKIE[$pageName];
    }
    return $page;
}