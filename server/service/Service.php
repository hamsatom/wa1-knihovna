<?php
/**
 * This is a service for validation and persisting all entities
 *
 * This class is used for validating user data from View and persisting all entities to database.
 *
 * @package      server
 * @subpackage   service
 * @category     Service
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses DB.php, db
 */

/**
 * This methods is responsible for validating author fields and persisting new author to database
 *
 * @param string $rawFirstName User input for author's first name
 * @param string $rawSurname User input for author's surname
 * @param string $rawEmail User input for author's email
 * @return string Error message if entity was not valid or there was error while persisting, empty string otherwise
 * @uses DB.php, db
 */
function persistAuthor($rawFirstName, $rawSurname, $rawEmail)
{
    if (!filter_var($rawEmail, FILTER_VALIDATE_EMAIL) || empty($rawFirstName) || empty($rawSurname)) {
        return "Autor musí mít validní email, jméno i přijmení";
    }

    $fields = array($rawFirstName, $rawSurname);
    mb_internal_encoding("UTF-8");
    for ($i = 0; $i < count($fields); $i++) {
        $field = $fields[$i];
        if (strlen($field) < 2 || mb_substr($field, 0, 1, "UTF-8") == mb_strtolower(mb_substr($field, 0, 1, "UTF-8"), "UTF-8")
            || !preg_match('/^\p{L}+$/u', mb_substr($field, 1, NULL, "UTF-8"))) {
            return "Jméno i příjmení musí začínat velkým písmenem a pak obsahovat pouze písmena";
        }
    }

    try {
        require('./../../server/db/DB.php');

        $insert = $db->prepare('INSERT INTO AUTHORS (first_name, surname, email) VALUES (?, ?, ?)');
        $insert->execute(array($rawFirstName, $rawSurname, $rawEmail));
    } catch (PDOException $e) {
        return "Autor se stejným emailem už existuje";
    }

    return "";
}

/**
 * This methods is responsible for validating book fields and persisting new author to database
 *
 * @param string $rawDate User input for book's release date
 * @param string $rawName User input for book's name
 * @param string $rawGenre User input for book's genre
 * @param string $rawIsbn User input for book's isbn
 * @return string Error message if entity was not valid or there was error while persisting, empty string otherwise
 * @uses DB.php, db
 */
function persistBook($rawDate, $rawName, $rawGenre, $rawIsbn)
{
    if (empty($rawDate) || empty($rawName) || empty($rawGenre) || empty($rawIsbn)) {
        return "Datum, jméno, žánr i ISBN musí být vyplněno";
    } else if (!ctype_digit($rawIsbn) || strlen($rawIsbn) > 13 || strlen($rawIsbn) < 9) {
        return "ISBN musí obsahovat vyhrádně 9 - 13 číslic";
    }

    try {
        require('./../../server/db/DB.php');

        $insert = $db->prepare('INSERT INTO BOOKS (name, isbn, date, genre) VALUES (?, ?, ?, ?)');
        $insert->execute(array($rawName, $rawIsbn, $rawDate, $rawGenre));
    } catch (PDOException $e) {
        return "ISBN není unikátní";
    }

    return "";
}

/**
 * This methods is responsible for validating library fields and persisting new author to database
 *
 * @param string $rawName User input for library's name
 * @param string $rawAddress User input for library's address
 * @return string Error message if entity was not valid or there was error while persisting, empty string otherwise
 * @uses DB.php, db
 */
function persistLibrary($rawName, $rawAddress)
{
    if (empty($rawName) || empty($rawAddress)) {
        return "Jméno i adresa musí být vyplněna";
    }

    try {
        require('./../../server/db/DB.php');

        $insert = $db->prepare('INSERT INTO LIBRARIES (name, address) VALUES (?, ?)');
        $insert->execute(array($rawName, $rawAddress));
    } catch (PDOException $e) {
        return "Ukládání do databáze selhalo";
    }

    return "";
}

/**
 * This methods is responsible for validating publisher fields and persisting new author to database
 *
 * @param string $rawName User input for publisher's name
 * @param string $rawAddress User input for publisher's address
 * @return string Error message if entity was not valid or there was error while persisting, empty string otherwise
 * @uses DB.php, db
 */
function persistPublisher($rawName, $rawAddress)
{
    if (empty($rawName) || empty($rawAddress)) {
        return "Jméno i adresa musí být vyplněna";
    }

    try {
        require('./../../server/db/DB.php');

        $insert = $db->prepare('INSERT INTO PUBLISHERS (name, address) VALUES (?, ?)');
        $insert->execute(array($rawName, $rawAddress));
    } catch (PDOException $e) {
        return "Ukládání do databáze selhalo";
    }

    return "";
}

/**
 * Verifies that login detail are not empty. Then salts and hashes password and inserts user into database.
 *
 * @param string $rawUsername User input of username
 * @param string $rawPassword User input of password
 * @return string Empty string if successful, error message otherwise
 * @uses DB.php, db
 */
function registerUser($rawUsername, $rawPassword)
{
    if (empty($rawUsername) || empty($rawPassword)) {
        return "Uživatelské jméno i heslo musí být vyplněno";
    }
    try {
        require('./server/db/DB.php');
        $insert = $db->prepare('INSERT INTO USERS (username, password) VALUES (?, ?)');
        $password = password_hash($rawPassword, PASSWORD_DEFAULT);
        $insert->execute(array($db->quote($rawUsername), $password));
    } catch (PDOException $e) {
        return "Přihlašovací jméno obsazeno";
    }
    return "";
}

/**
 * Finds user in database and compares provided password with the one stored in database.
 *
 * @param string $rawUsername User input of username
 * @param string $rawPassword User input of password
 * @return string Empty string if successful, error message if user not found or password is different
 * @uses DB.php, db
 */
function loginUser($rawUsername, $rawPassword)
{
    if (empty($rawUsername) || empty($rawPassword)) {
        return "Uživatelské jméno i heslo musí být vyplněno";
    }

    try {
        require('./server/db/DB.php');

        $insert = $db->prepare('SELECT * FROM USERS WHERE username=?');
        $insert->execute(array($db->quote($rawUsername)));
        $rows = $insert->fetchAll();
        $row = end($rows);

        if (!isset($row["password"]) || !password_verify($rawPassword, $row["password"])) {
            return "Špatné jméno nebo heslo";
        }

    } catch (PDOException $e) {
        return "Špatné jméno nebo heslo";
    }

    return "";
}

/**
 * Verifies that user is present in database
 *
 * @param string $rawUsername Username from cookie
 * @return bool true if user found in database, false if not found or error occurred
 * @uses DB.php, db
 */
function userExists($rawUsername)
{
    if (empty($rawUsername)) {
        return false;
    }

    try {
        require(__DIR__ . '/../db/DB.php');

        $insert = $db->prepare('SELECT COUNT(*) FROM USERS WHERE username=?');
        $insert->execute(array($db->quote($rawUsername)));
        $rows = $insert->fetchAll();

        return !empty($rows) && isset($rows[0]["COUNT(*)"]) && $rows[0]["COUNT(*)"] > 0;

    } catch (PDOException $e) {
        return false;
    }
}