<?php
/**
 * This is a view for login page and menu
 *
 * This view is responsible for displaying login form and menu.
 * Login form is displayed if user doesn't logged cookie set. Menu if the cookie is set.
 * User data from this view are passed to service.
 *
 * @package      project's root folder
 * @category     View
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses Service.php
 */

$errorMessage = "";
if (!session_start()) {
    $errorMessage = "Chyba servru, zkuste to znova";
}

$rawUsername = "";
$valid = false;

if (isset($_COOKIE["sessionId"]) && isset($_SESSION[$_COOKIE["sessionId"]])) {
    require_once('./server/service/Service.php');
    $valid = userExists($_SESSION[$_COOKIE["sessionId"]]);
} else if (isset($_POST["username"]) && isset($_POST["password"]) && !empty($_POST["username"])) {
    $rawUsername = $_POST["username"];
    $rawPassword = $rawUsername . $_POST["password"];

    require_once('./server/service/Service.php');

    if (isset($_POST["register"])) {
        $errorMessage = registerUser($rawUsername, $rawPassword);
    }
    if (empty($errorMessage)) {
        $errorMessage = loginUser($rawUsername, $rawPassword);
    }
    $valid = empty($errorMessage);
    if ($valid) {
        $sessionId = session_id();
        setcookie("sessionId", $sessionId, 0);
        $_SESSION[$sessionId] = $rawUsername;
    }
} else {
    $valid = false;
}

if ($valid) {
    $menu = '<ul>
                <li><a href="client/php/AuthorView.php">Autoři</a></li>
                <li><a href="client/php/BookView.php">Knihy</a></li>
                <li><a href="client/php/LibraryView.php">Knihovny</a></li>
                <li><a href="client/php/PublisherView.php">Nakladatelství</a></li>
              </ul>';
} else {
    $menu = '<form method="post" action="index.php" id="loginForm" onsubmit="return validateTextInput(\'loginForm\')">
              <div class="invalid">' . $errorMessage . '</div> 

               <label for="indexUsername"><strong>Uživatelské jméno:</strong></label>
                <input type="text" name="username" id="indexUsername" required="required" title="uživatelské jméno" 
                onblur="validateTextInput(\'loginForm\')"
                value="' . htmlspecialchars($rawUsername, ENT_QUOTES) . '">
             
              <label for="indexPassword"><strong>Heslo:</strong></label>
              <input type="password" name="password" id="indexPassword" required="required" title="heslo" 
              onblur="validateTextInput(\'loginForm\')">

               <input type="submit" value="Přihlásit">
               <input type="submit" value="Registrovat" name="register">
     </form>';
}
?>


<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <title>Knihovní systém</title>
  <link rel="stylesheet" type="text/css" href="client/css/GlobalStyle.css">
  <link rel="stylesheet" type="text/css" href="client/css/IndexStyle.css">
  <script src="./client/javascript/GlobalScript.js"></script>
</head>
<body>
<header>
  <h1>
    Knihovní systém
  </h1>
</header>

<?php echo $menu; ?>
<ul>
  <li><a href="./documentation/Menu.html">Dokumentace</a></li>
</ul>

<footer>
  <strong>Autor:</strong> Tomáš Hamsa &nbsp;
  <strong>Předmět:</strong> WA1 &nbsp;
  <strong>Semestr:</strong> B171 &nbsp;
</footer>

</body>
</html>