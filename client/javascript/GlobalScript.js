/**
 * Validates that input field is not empty and contains @ if the input field is of type email.
 *
 * If the input field is empty than this method adds class invalid to it. If the input field is not empty than removes class invalid from class list.
 *
 * @param id string Id of for where the input validation will be applied
 * @returns {boolean} true if all fields are non empty, false otherwise
 * @uses AuthorView.php, BookView.php, LibraryView.php, PublisherView.php, index.php
 * @function
 */
function validateTextInput(id) {
  var form = document.getElementById(id);
  if (!form) {
    return true;
  }

  var allValid = true;

  var contents = form.getElementsByTagName("input");
  for (var i = 0; i < contents.length; ++i) {
    var content = contents[i];
    var fieldInvalid = !content.value.trim();
    if (content.getAttribute("type") === "email" && content.value.indexOf("@")
        === -1) {
      fieldInvalid = true;
    }

    if (fieldInvalid) {
      allValid = false;
      content.classList.add("invalid");
    } else {
      content.classList.remove("invalid");
    }
  }

  return allValid;
}

/**
 * Sets ajax response html text to table. The table is selected by id items from page that called ajax request
 *
 * @param e ajax event
 * @uses AuthorView.php, BookView.php, LibraryView.php, PublisherView.php
 * @function
 */
function ajaxResponse(e) {
  document.getElementById("items").innerHTML = e.target.responseText;
}

/**
 * Sends ajax request to filter specified by table name. On response sets result to page's table.
 *
 * @param limit int Number of displayed records per page
 * @param page int Number of page
 * @uses AuthorFilter.php, BookFilter.php, LibraryFilter.php, PublisherFilter.php
 * @function
 */
function ajaxRequest(limit, page) {
  var form = document.forms[1];
  var query = "?limit=" + limit + "&page=" + page;

  var contents = form.getElementsByTagName("input");
  for (var i = 0; i < contents.length; ++i) {
    var content = contents[i];
    var str = content.value.trim();
    if (str && str.length && str !== "Hledat") {
      query = query + "&" + encodeURIComponent(content.name) + "="
          + encodeURIComponent(str);
    }
  }

  var url = "./../../server/filter/" + form.id + ".php" + query;

  var request = new XMLHttpRequest();
  request.open("get", url, true);
  request.send();
  request.addEventListener("load", ajaxResponse);
}