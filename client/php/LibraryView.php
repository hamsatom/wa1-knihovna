<?php
/**
 * This is a view for Library entity.
 *
 * This view sets data to input fields in create section.
 * Processes user data and sends them to service. This view also calls filter and displays page and limit.
 * This view also sets data to table.
 *
 * @package      client
 * @subpackage   php
 * @category     View
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses checkLogin(), getLimit(), persistLibrary(), getPageNumber(), doLibraryCount(), doLibraryFilter(), validateTextInput(), ajaxRequest(()
 */

require('./../../server/Utils.php');
checkLogin();

$limit = getLimit('libraryLimit');
$errorMsg = "";
$name = "";
$address = "";

if (isset($_POST["name"]) && isset($_POST["address"])) {
    $rawName = $_POST["name"];
    $rawAddress = $_POST["address"];

    require_once('./../../server/service/Service.php');
    $errorMsg = persistLibrary($rawName, $rawAddress);

    if (empty($errorMsg)) {
        $name = "";
        $address = "";
    } else {
        $name = htmlspecialchars($rawName, ENT_QUOTES);
        $address = htmlspecialchars($rawAddress, ENT_QUOTES);
    }
} else {
    $errorMsg = "Jméno i adresa musí být vyplněna";
}

require('./../../server/filter/Filter.php');
$maxPage = doLibraryCount($limit);
$page = getPageNumber("libraryPage", $maxPage);
$table = doLibraryFilter(($page - 1) * $limit, $limit);
?>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <title>Knihovny</title>
  <link rel="stylesheet" type="text/css" href="../css/GlobalStyle.css">
  <script src="../javascript/GlobalScript.js"></script>
</head>


<body>
<header><h1>Knihovny</h1></header>

<form method="post" action="LibraryView.php" id="libraryCreateForm"
      onsubmit="return validateTextInput('libraryCreateForm')">
  <fieldset data-role="collapsible">
    <legend>Přidat knihovnu</legend>

    <div class=invalid> <?php echo $errorMsg; ?> </div>

    <label for="libraryAddName">Název:</label>
    <input type="text" id="libraryAddName" required="required"
           onblur="validateTextInput('libraryCreateForm')"
           title="název" name="name" value="<?php echo $name; ?>">

    <label for="libraryAddAddress">Adresa:</label>
    <input type="text" id="libraryAddAddress" required="required"
           onblur="validateTextInput('libraryCreateForm')"
           title="adresa" name="address" value="<?php echo $address; ?>">

    <input type="submit" data-inline="true" value="Přidat">
  </fieldset>
</form>

<form method="get" action="LibraryView.php" id="LibraryFilter">
  <fieldset data-role="collapsible">
    <legend>Hledat</legend>

    <label for="librarySearchName">Název:</label>
    <input type="text" id="librarySearchName" name="name"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <label for="librarySearchAddress">Adresa:</label>
    <input type="text" id="librarySearchAddress" name="address"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <input type="submit" data-inline="true" value="Hledat">
  </fieldset>
</form>
<form method="get" action="LibraryView.php" id="libraryLimitForm"
      onsubmit="return validateTextInput('libraryLimitForm')">
  <fieldset data-role="collapsible">
    <legend>Počet položek</legend>
    <label for="itemsLimit">Počet položek na stránce:</label>
    <input type="number" min="1" value="<?php echo $limit; ?>" name="limit" title="limit"
           id="itemsLimit" required="required" onblur="validateTextInput('libraryLimitForm')">
    <input type="submit" value="Limitovat">
  </fieldset>
</form>
<form method="get" action="LibraryView.php" id="libraryPageForm"
      onsubmit="return validateTextInput('libraryPageForm')">
  <fieldset>
    <legend>Stránka</legend>
    <input type="number" min="1" max="<?php echo $maxPage; ?>" value="<?php echo $page; ?>"
           name="page" title="stránka" required="required"
           onblur="validateTextInput('libraryPageForm')">
    <input type="submit" value="Přejít">
  </fieldset>
</form>

<table id="tableLibrary">
  <thead>
  <tr>
    <th>Název</th>
    <th>Adresa</th>
  </tr>
  </thead>
  <tbody id="items">
  <?php echo $table; ?>
  </tbody>
</table>

</body>

</html>
