<?php
/**
 * This is a view for Author entity.
 *
 * This view sets data to input fields in create section.
 * Processes user data and sends them to service. This view also calls filter and displays page and limit.
 * This view also sets data to table.
 *
 * @package      client
 * @subpackage   php
 * @category     View
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses checkLogin(), getLimit(), persistAuthor(), getPageNumber(), doAuthorCount(), doAuthorFilter(), validateTextInput(), ajaxRequest(()
 */

setlocale(LC_ALL, 'cs_CZ.UTF-8', 'cs_CZ', 'czech', 'csy', 'cze');
require('./../../server/Utils.php');
checkLogin();

$limit = getLimit('authorLimit');
$errorMsg = "";
$firstName = "";
$surname = "";
$email = "";

if (isset($_POST["first_name"]) && isset($_POST["surname"]) && isset($_POST["email"])) {
    $rawFirstName = $_POST["first_name"];
    $rawSurname = $_POST["surname"];
    $rawEmail = $_POST["email"];

    require_once('./../../server/service/Service.php');
    $errorMsg = persistAuthor($rawFirstName, $rawSurname, $rawEmail);

    if (empty($errorMsg)) {
        $firstName = "";
        $surname = "";
        $email = "";
    } else {
        $firstName = htmlspecialchars($rawFirstName, ENT_QUOTES);
        $surname = htmlspecialchars($rawSurname, ENT_QUOTES);
        $email = htmlspecialchars($rawEmail, ENT_QUOTES);
    }
} else {
    $errorMsg = "Jméno, přijmení i email autora musí být vyplněno";
}

require('./../../server/filter/Filter.php');
$maxPage = doAuthorCount($limit);
$page = getPageNumber("authorPage", $maxPage);
$table = doAuthorFilter(($page - 1) * $limit, $limit);
?>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <title>Autoři</title>
  <link rel="stylesheet" type="text/css" href="../css/GlobalStyle.css">
  <script src="../javascript/GlobalScript.js"></script>
</head>

<body>
<header><h1>Autoři</h1></header>

<form method="post" action="AuthorView.php" id="authorCreateForm"
      onsubmit="return validateTextInput('authorCreateForm')">
  <fieldset data-role="collapsible">
    <legend>Přidat autora</legend>

    <div class=invalid> <?php echo $errorMsg; ?> </div>

    <label for="authorAddFirstName">Jméno:</label>
    <input type="text" id="authorAddFirstName" name="first_name" required="required"
           pattern="[A-Ž]{1}[A-Ža-ž]*" title="Jméno s velkým počátečním písmenem"
           value="<?php echo $firstName ?>" onblur="validateTextInput('authorCreateForm')">

    <label for="authorAddSurname">Příjmení:</label>
    <input type="text" id="authorAddSurname" required="required"
           onblur="validateTextInput('authorCreateForm')"
           pattern="[A-Ž]{1}[A-Ža-ž]*" name="surname" value="<?php echo $surname ?>"
           title="Přijímení s velkým počátečním písmenem">

    <label for="authorAddEmail">Email:</label>
    <input type="email" id="authorAddEmail" required="required" name="email"
           value="<?php echo $email ?>" onblur="validateTextInput('authorCreateForm')"
           title="email">

    <input type="submit" data-inline="true" value="Přidat">
  </fieldset>
</form>

<form method="get" action="AuthorView.php" id="AuthorFilter">
  <fieldset data-role="collapsible">
    <legend>Hledat</legend>

    <label for="authorSearchFirstName">Jméno:</label>
    <input type="text" id="authorSearchFirstName" name="first_name"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <label for="authorSearchSurname">Příjmení:</label>
    <input type="text" id="authorSearchSurname" name="surname"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <label for="authorSearchEmail">Email:</label>
    <input type="text" id="authorSearchEmail" name="email"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <input type="submit" data-inline="true" value="Hledat">
  </fieldset>
</form>
<form method="get" action="AuthorView.php" id="authorLimitForm"
      onsubmit="return validateTextInput('authorLimitForm')">
  <fieldset data-role="collapsible">
    <legend>Počet položek</legend>
    <label for="itemsLimit">Počet položek na stránce:</label>
    <input type="number" min="1" value="<?php echo $limit; ?>"
           name="limit" title="limit" id="itemsLimit" required="required"
           onblur="validateTextInput('authorLimitForm')">
    <input type="submit" value="Limitovat">
  </fieldset>
</form>
<form method="get" action="AuthorView.php" id="authorPageForm"
      onsubmit="return validateTextInput('authorPageForm')">
  <fieldset>
    <legend>Stránka</legend>
    <input type="number" min="1" max="<?php echo $maxPage; ?>" value="<?php echo $page; ?>"
           name="page" title="stránka" required="required"
           onblur="validateTextInput('authorPageForm')">
    <input type="submit" value="Přejít">
  </fieldset>
</form>

<table id="tableAuthor">
  <thead>
  <tr>
    <th>Jméno</th>
    <th>Přijímení</th>
    <th>Email</th>
  </tr>
  </thead>
  <tbody id="items">
  <?php echo $table; ?>
  </tbody>
</table>


</body>

</html>
