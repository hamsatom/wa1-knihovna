<?php
/**
 * This is a view for Publisher entity.
 *
 * This view sets data to input fields in create section.
 * Processes user data and sends them to service. This view also calls filter and displays page and limit.
 * This view also sets data to table.
 *
 * @package      client
 * @subpackage   php
 * @category     View
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses checkLogin(), getLimit(), persistPublisher(), getPageNumber(), doPublisherCount(), doPublisherFilter(), validateTextInput(), ajaxRequest(()
 */

require('./../../server/Utils.php');
checkLogin();

$limit = getLimit('publisherLimit');
$errorMsg = "";
$name = "";
$address = "";

if (isset($_POST["name"]) && isset($_POST["address"])) {
    $rawName = $_POST["name"];
    $rawAddress = $_POST["address"];

    require_once('./../../server/service/Service.php');
    $errorMsg = persistPublisher($rawName, $rawAddress);

    if (empty($errorMsg)) {
        $name = "";
        $address = "";
    } else {
        $name = htmlspecialchars($rawName, ENT_QUOTES);
        $address = htmlspecialchars($rawAddress, ENT_QUOTES);
    }
} else {
    $errorMsg = "Jméno i adresa musí být vyplněna";
}

require('./../../server/filter/Filter.php');
$maxPage = doPublisherCount($limit);
$page = getPageNumber("publisherPage", $maxPage);
$table = doPublisherFilter(($page - 1) * $limit, $limit);
?>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <title>Nakladatelství</title>
  <link rel="stylesheet" type="text/css" href="../css/GlobalStyle.css">
  <script src="../javascript/GlobalScript.js"></script>
</head>

<body>
<header><h1>Nakladatelství</h1></header>

<form method="post" action="PublisherView.php" id="publisherCreateForm"
      onsubmit="return validateTextInput('publisherCreateForm')">
  <fieldset data-role="collapsible">
    <legend>Přidat nakladatelství</legend>

    <div class=invalid> <?php echo $errorMsg; ?> </div>

    <label for="publisherAddName">Název:</label>
    <input type="text" id="publisherAddName" required="required"
           onblur="validateTextInput('publisherCreateForm')"
           title="název" name="name" value="<?php echo $name; ?>">

    <label for="publisherAddAddress">Adresa:</label>
    <input type="text" id="publisherAddAddress" required="required"
           onblur="validateTextInput('publisherCreateForm')"
           title="adresa" name="address" value="<?php echo $address; ?>">

    <input type="submit" data-inline="true" value="Přidat">
  </fieldset>
</form>

<form method="get" action="PublisherView.php" id="PublisherFilter">
  <fieldset data-role="collapsible">
    <legend>Hledat</legend>

    <label for="publisherSearchName">Název:</label>
    <input type="text" id="publisherSearchName" name="name"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <label for="publisherSearchAddress">Adresa:</label>
    <input type="text" id="publisherSearchAddress" name="address"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <input type="submit" data-inline="true" value="Hledat">
  </fieldset>
</form>
<form method="get" action="PublisherView.php" id="publisherLimitForm"
      onsubmit="return validateTextInput('publisherLimitForm')">
  <fieldset data-role="collapsible">
    <legend>Počet položek</legend>
    <label for="itemsLimit">Počet položek na stránce:</label>
    <input type="number" min="1" value="<?php echo $limit; ?>" name="limit"
           title="limit" id="itemsLimit" required="required"
           onblur="validateTextInput('publisherLimitForm')"> <input type="submit" value="Limitovat">
  </fieldset>
</form>
<form method="get" action="PublisherView.php" id="publisherPageForm"
      onsubmit="return validateTextInput('publisherPageForm')">
  <fieldset>
    <legend>Stránka</legend>
    <input type="number" min="1" max="<?php echo $maxPage; ?>" value="<?php echo $page; ?>"
           name="page" title="stránka" required="required"
           onblur="validateTextInput('publisherPageForm')">
    <input type="submit" value="Přejít">
  </fieldset>
</form>

<table id="tablePublisher">
  <thead>
  <tr>
    <th>Název</th>
    <th>Adresa</th>
  </tr>
  </thead>
  <tbody id="items">
  <?php echo $table; ?>
  </tbody>
</table>

</body>

</html>
