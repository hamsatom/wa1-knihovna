<?php
/**
 * This is a view for Book entity.
 *
 * This view sets data to input fields in create section.
 * Processes user data and sends them to service. This view also calls filter and displays page and limit.
 * This view also sets data to table.
 *
 * @package      client
 * @subpackage   php
 * @category     View
 * @author       Tomáš Hamsa <hamsatom@fel.cvut.cz>
 * @filesource
 * @uses checkLogin(), getLimit(), persistBook(), getPageNumber(), doBookCount(), doBookFilter(), validateTextInput(), ajaxRequest(()
 */

require('./../../server/Utils.php');
checkLogin();

$limit = getLimit('bookLimit');
$errorMsg = "";
$name = "";
$isbn = "";
$date = "";
$genre = "";

if (isset($_POST["name"]) && isset($_POST["isbn"]) && isset($_POST["date"]) && isset($_POST["genre"])) {
    $rawName = $_POST["name"];
    $rawIsbn = $_POST["isbn"];
    $rawDate = $_POST["date"];
    $rawGenre = $_POST["genre"];

    require_once('./../../server/service/Service.php');
    $errorMsg = persistBook($rawDate, $rawName, $rawGenre, $rawIsbn);

    if (empty($errorMsg)) {
        $name = "";
        $isbn = "";
        $date = "";
        $genre = "";
    } else {
        $name = htmlspecialchars($rawName, ENT_QUOTES);
        $isbn = htmlspecialchars($rawIsbn, ENT_QUOTES);
        $date = htmlspecialchars($rawDate, ENT_QUOTES);
        $genre = htmlspecialchars($rawGenre, ENT_QUOTES);
    }
} else {
    $errorMsg = "Datum, jméno, žánr i ISBN musí být vyplněno";
}

require('./../../server/filter/Filter.php');
$maxPage = doBookCount($limit);
$page = getPageNumber("bookPage", $maxPage);
$table = doBookFilter(($page - 1) * $limit, $limit);
?>

<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <title>Knihy</title>
  <link rel="stylesheet" type="text/css" href="../css/GlobalStyle.css">
  <script src="../javascript/GlobalScript.js"></script>
</head>

<body>
<header><h1>Knihy</h1></header>

<form method="post" action="BookView.php" id="bookCreateForm"
      onsubmit="return validateTextInput('bookCreateForm')">
  <fieldset data-role="collapsible">
    <legend>Přidat knihu</legend>

    <div class=invalid> <?php echo $errorMsg; ?> </div>

    <label for="bookAddName">Název:</label>
    <input type="text" id="bookAddName" required="required" name="name"
           onblur="validateTextInput('bookCreateForm')"
           title="název" value="<?php echo $name; ?>">

    <label for="bookAddIsbn">ISBN:</label>
    <input type="number" min="100000000" max="9999999999999" id="bookAddIsbn" required="required"
           onblur="validateTextInput('bookCreateForm')" value="<?php echo $isbn; ?>" name="isbn"
           title="ISBN">

    <label for="bookAddReleaseDate">Datum vydání:</label>
    <input type="date" data-polyfill="all" id="bookAddReleaseDate" required="required" name="date"
           onblur="validateTextInput('bookCreateForm')" title="datum" value="<?php echo $date; ?>">

    <label for="bookAddGenre">Žánr:</label>
    <input type="text" id="bookAddGenre" required="required" name="genre"
           onblur="validateTextInput('bookCreateForm')" title="žánr" value="<?php echo $genre; ?>">

    <input type="submit" data-inline="true" value="Přidat">
  </fieldset>
</form>

<form method="get" action="BookView.php" id="BookFilter">
  <fieldset data-role="collapsible">
    <legend>Hledat</legend>

    <label for="bookSearchName">Název:</label>
    <input type="text" name="name" id="bookSearchName"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <label for="bookSearchIsbn">ISBN:</label>
    <input type="number" name="isbn" id="bookSearchIsbn"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <label for="bookSearchReleaseDate">Datum vydání:</label>
    <input type="date" data-polyfill="all" name="date" id="bookSearchReleaseDate"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <label for="bookSearchGenre">Žánr:</label>
    <input type="text" id="bookSearchGenre" name="genre"
           oninput="ajaxRequest(<?php echo $limit; ?>, <?php echo $page; ?>)">

    <input type="submit" data-inline="true" value="Hledat">
  </fieldset>
</form>
<form method="get" action="BookView.php" id="bookLimitForm"
      onsubmit="return validateTextInput('bookLimitForm')">
  <fieldset data-role="collapsible">
    <legend>Počet položek</legend>
    <label for="itemsLimit">Počet položek na stránce:</label>
    <input type="number" min="1" value="<?php echo $limit; ?>" name="limit" title="limit"
           id="itemsLimit" required="required" onblur="validateTextInput('bookLimitForm')">
    <input type="submit" value="Limitovat">
  </fieldset>
</form>
<form method="get" action="BookView.php" id="bookPageForm"
      onsubmit="return validateTextInput('bookPageForm')">
  <fieldset>
    <legend>Stránka</legend>
    <input type="number" min="1" max="<?php echo $maxPage; ?>" value="<?php echo $page; ?>"
           name="page" title="stránka" required="required"
           onblur="validateTextInput('bookPageForm')">
    <input type="submit" value="Přejít">
  </fieldset>
</form>

<table id="tableBook">
  <thead>
  <tr>
    <th>Název</th>
    <th>ISBN</th>
    <th>Datum vydání</th>
    <th>Žánr</th>
  </tr>
  </thead>
  <tbody id="items">
  <?php echo $table; ?>
  </tbody>
</table>

</body>

</html>
