var a00020 =
[
    [ "_createQuery", "db/db2/a00020.html#a28a09719b16094c14601a7eea932a726", null ],
    [ "_doCount", "db/db2/a00020.html#a14d5ca64315e068646d5b02e4dbfbfad", null ],
    [ "_doFilter", "db/db2/a00020.html#a79e194796b604796d1d0d1ac4b0cb86a", null ],
    [ "doAuthorCount", "db/db2/a00020.html#a81e544dabd6c4c58a9d56a27a54afba3", null ],
    [ "doAuthorFilter", "db/db2/a00020.html#aff9f09187d7b306886b688002f2ee4c9", null ],
    [ "doBookCount", "db/db2/a00020.html#a2c8df7d377ec7fb229897cb195696d1b", null ],
    [ "doBookFilter", "db/db2/a00020.html#a141ee121b110e72487dfa44821a64e08", null ],
    [ "doLibraryCount", "db/db2/a00020.html#af55a79dbbc14c13f309c158ca14e6805", null ],
    [ "doLibraryFilter", "db/db2/a00020.html#a3c4f2e0982cd858d9c3c2a4d64946661", null ],
    [ "doPublisherCount", "db/db2/a00020.html#ad9c3b8441b2479b63f152de081693f26", null ],
    [ "doPublisherFilter", "db/db2/a00020.html#a6f13bb9346c22f35b9a363f82cfc56f8", null ]
];